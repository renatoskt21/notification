# Email Notification Library using phpMailer

Send by email using a phpmailer library. Doing a decompilation action is essential for any system.

To do a library installation, run the following command:

``` sh
composer require gustavoweb/composer_test
```

To make use of the library, simply require the autoload of the composer, invoke and call the method method:

``` sh
<? php

Demand __DIR__. '/vendor/autoload.php';

USE Notification\Email;

$mail = new Email (2, "mail.host.com", "your@email.com", "your pass", "smtp secure (tls / ssl)", "from@email.com", "From the name");

$email-> sendEmail ("Subject", "Content", "reply@email.com", "Repetition Name", "address@email.com", "Address Name");
```

Note that the entire configuration of the email is being used by the magic method builder! Once the builder is invoked within your application, your system will be able to perform the shots.

### Developers
* [Gustavo Web] - Highlights library and course teacher Composer in Practice!
* [Robson V. Leite] - CEO and founder of UpInside Trainings
* [UpInside Treinamentos] - Official site of your digital marketing and programming school
* [phpMailer] - Lib to send email

License
----

MIT

** One more course from UpInside Trainings, make good use! **

[//]: #
[Renato Pereira]: <mailto: renatops1991@gmail.com>
[Renatops]: <https://www.renatopereira.com.br>
[phpMailer]: <https://github.com/PHPMailer/PHPMailer>
